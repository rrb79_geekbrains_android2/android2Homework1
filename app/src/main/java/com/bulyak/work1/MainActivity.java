package com.bulyak.work1;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<String> elements = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initElements();
        listView = findViewById(R.id.list_activity_man);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, elements);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
    }

    private void initElements() {
        for (int i = 0; 10 > i; i++) {
            elements.add("Auto init ");
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_con_edit:
                editElements(info.position);
                return true;
            case R.id.menu_con_delete:
                deleteElements(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_ad_add:
                addElements();
                actionMenu("Add");
                return true;
            case R.id.menu_ab_delete:
                //delete action
                clearList();
                actionMenu("Delete");
                return true;
            case R.id.menu_ad_edit:
                //edit action
                actionMenu("Edit");
                return true;
        }
        return true;
    }

    private void deleteElements(int position) {
        elements.remove(position);
        adapter.notifyDataSetChanged();
    }

    private void editElements(int position) {
        elements.set(position, "Edit element" + position);
        adapter.notifyDataSetChanged();

    }

    private void addElements() {
        elements.add("new elements add");
        adapter.notifyDataSetChanged();
    }

    private void clearList() {
        elements.clear();
        adapter.notifyDataSetChanged();
    }

    private void actionMenu(String massege) {
        Toast toast = Toast.makeText(getApplicationContext(),
                "you clicked menu " + massege, Toast.LENGTH_SHORT);
        toast.show();
    }
}



